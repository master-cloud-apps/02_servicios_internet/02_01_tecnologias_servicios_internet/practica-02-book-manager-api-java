package org.eyo.bookmanager.api;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.headers.Header;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.eyo.bookmanager.dtos.requests.CommentRequestDto;
import org.eyo.bookmanager.dtos.responses.CommentResponseDTO;
import org.eyo.bookmanager.models.Comment;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

public interface CommentAPI {

    @Operation(summary = "Create a comment")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "201",
                    description = "Comment created",
                    headers = @Header(name = "Location", description = "Location of the comment", schema =
                    @Schema(implementation = String.class))
            ),
            @ApiResponse(
                    responseCode = "400",
                    description = "Bad request",
                    content = @Content
            )
    })
    ResponseEntity<Void> createComment(@PathVariable Long bookId, @RequestBody CommentRequestDto comment);

    @Operation(summary = "Get a comment by id")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Comment selected",
                    content = @Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = Comment.class))
            ),
            @ApiResponse(
                    responseCode = "400",
                    description = "Bad request",
                    content = @Content
            ),
            @ApiResponse(
                    responseCode = "404",
                    description = "Comment not found",
                    content = @Content
            )
    })
    ResponseEntity<CommentResponseDTO> getCommentById(@PathVariable Long bookId, @PathVariable Long commentId);

    @Operation(summary = "Delete a comment by id")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "204",
                    description = "Comment deleted"
            ),
            @ApiResponse(
                    responseCode = "400",
                    description = "Bad request",
                    content = @Content
            ),
            @ApiResponse(
                    responseCode = "404",
                    description = "Book not found",
                    content = @Content
            )
    })
    ResponseEntity<Void> deleteCommentOverBook(@PathVariable Long bookId, @PathVariable Long commentId);
}
