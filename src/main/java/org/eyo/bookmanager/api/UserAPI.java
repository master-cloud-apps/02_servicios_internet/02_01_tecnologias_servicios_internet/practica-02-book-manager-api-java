package org.eyo.bookmanager.api;

import org.eyo.bookmanager.dtos.requests.UserRequestDTO;
import org.eyo.bookmanager.dtos.responses.UserCommentsResponseDTO;
import org.eyo.bookmanager.dtos.responses.UserResponseDTO;
import org.springframework.http.ResponseEntity;

import java.util.Collection;

public interface UserAPI {
    ResponseEntity<Void> createUSer(UserRequestDTO user);

    ResponseEntity<Collection<UserResponseDTO>> getUsers();

    ResponseEntity<Void> deleteUserById(Long userId);

    ResponseEntity<UserResponseDTO> updateUser(Long userId, UserRequestDTO updatedUser);

    ResponseEntity<UserResponseDTO> getUserById(Long userId);

    ResponseEntity<Collection<UserCommentsResponseDTO>> getUserComments(Long userId);
}
