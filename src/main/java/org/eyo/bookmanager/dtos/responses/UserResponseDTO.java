package org.eyo.bookmanager.dtos.responses;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.eyo.bookmanager.models.User;

@AllArgsConstructor
@Getter
public class UserResponseDTO {

    public UserResponseDTO(User user) {
        this.id = user.getId();
        this.nick = user.getNick();
        this.email = user.getEmail();
    }

    private Long id;
    private String nick;
    private String email;
}
