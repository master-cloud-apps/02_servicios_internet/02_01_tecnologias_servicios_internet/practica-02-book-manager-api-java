package org.eyo.bookmanager.dtos.responses;

import lombok.Getter;
import org.eyo.bookmanager.models.Comment;

@Getter
public class CommentResponseDTO {

    public CommentResponseDTO(Comment comment) {
        this.id = comment.getId();
        this.content = comment.getContent();
        this.punctuation = comment.getPunctuation();
        this.user = new UserResponseDTO(comment.getUser());
        this.book = new BookResponseDTO(comment.getBook());
    }

    private Long id;
    private String content;
    private Float punctuation;
    private UserResponseDTO user;
    private BookResponseDTO book;
}
