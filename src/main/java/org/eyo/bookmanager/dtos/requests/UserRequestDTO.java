package org.eyo.bookmanager.dtos.requests;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.eyo.bookmanager.models.User;

@Getter
@AllArgsConstructor
public class UserRequestDTO {

    public UserRequestDTO(User user) {
        this.nick = user.getNick();
        this.email = user.getEmail();
    }

    private String nick;
    private String email;
}
