package org.eyo.bookmanager.repositories;

import org.eyo.bookmanager.models.Book;
import org.eyo.bookmanager.models.Comment;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicLong;

@Profile("test")
@Repository
public class InMemoryBookRepository implements BookRepository{
    private AtomicLong nextId = new AtomicLong(1);
    private ConcurrentMap<Long, Book> books = new ConcurrentHashMap<>();

    public Book save(Book book) {
        if (book.getId() == null || book.getId() == 0) {
            book.setId(nextId.getAndIncrement());
        }
        this.books.put(book.getId(), book);
        return book;
    }

    public Collection<Book> getAll() {
        return this.books.values();
    }

    public Book findById(Long bookId) {
        return this.books.get(bookId);
    }

    public void deleteById(Long bookId) {
        this.books.remove(bookId);
    }

    public void addCommentToBook(Long bookId, Comment comment) {
        Book bookToComment = this.books.get(bookId);
        bookToComment.getComments().add(comment);
    }
}
