package org.eyo.bookmanager.repositories.db;

import org.eyo.bookmanager.models.Book;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BookJpaRepository extends JpaRepository<Book, Long> {
}
