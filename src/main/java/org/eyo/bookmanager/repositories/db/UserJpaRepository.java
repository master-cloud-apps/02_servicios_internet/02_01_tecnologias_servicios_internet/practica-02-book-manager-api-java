package org.eyo.bookmanager.repositories.db;

import org.eyo.bookmanager.models.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserJpaRepository extends JpaRepository<User, Long> {
    User findUserByNick(String nick);
}
