package org.eyo.bookmanager.repositories;

import org.eyo.bookmanager.models.Book;
import org.eyo.bookmanager.models.Comment;

import java.util.Collection;

public interface BookRepository {

    Book save(Book book);

    Collection<Book> getAll();

    Book findById(Long bookId);

    void deleteById(Long bookId);

    void addCommentToBook(Long bookId, Comment comment);
}
