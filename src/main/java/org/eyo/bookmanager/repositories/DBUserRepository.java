package org.eyo.bookmanager.repositories;

import org.eyo.bookmanager.models.User;
import org.eyo.bookmanager.repositories.db.UserJpaRepository;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
@Profile("db")
public class DBUserRepository implements UserRepository {

    private UserJpaRepository userJpaRepository;

    public DBUserRepository(UserJpaRepository userJpaRepository) {
        this.userJpaRepository = userJpaRepository;
    }

    @Override
    public void save(User user) {
        this.userJpaRepository.save(user);
    }

    @Override
    public Collection<User> getAll() {
        return this.userJpaRepository.findAll();
    }

    @Override
    public User findById(Long userId) {
        return this.userJpaRepository.findById(userId).orElse(null);
    }

    @Override
    public void deleteById(Long userId) {
        this.userJpaRepository.deleteById(userId);
    }

    @Override
    public User getUserByNick(String nick) {
        return this.userJpaRepository.findUserByNick(nick);
    }
}
