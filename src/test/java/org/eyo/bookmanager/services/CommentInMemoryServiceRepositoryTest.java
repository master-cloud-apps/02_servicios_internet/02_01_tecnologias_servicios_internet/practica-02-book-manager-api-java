package org.eyo.bookmanager.services;

import org.eyo.bookmanager.BookBuilder;
import org.eyo.bookmanager.CommentBuilder;
import org.eyo.bookmanager.models.Book;
import org.eyo.bookmanager.models.Comment;
import org.eyo.bookmanager.repositories.BookRepository;
import org.eyo.bookmanager.repositories.CommentRepository;
import org.eyo.bookmanager.repositories.InMemoryBookRepository;
import org.eyo.bookmanager.repositories.InMemoryCommentRepository;
import org.junit.jupiter.api.Test;

import java.util.Collection;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

class CommentInMemoryServiceRepositoryTest {

    private BookRepository bookRepository = new InMemoryBookRepository();
    private CommentRepository commentRepository = new InMemoryCommentRepository();
    private BookService bookService = new BookServiceRepository(this.bookRepository);
    private CommentService commentService = new CommentServiceRepository(this.bookService, this.commentRepository);

    @Test
    void should_get_empty_comments_list(){
        assertEquals(0, this.commentService.getAll(-1L).size());
    }

    @Test
    void given_new_comment_with_id_zero_wehn_save_then_new_comment_change_id(){
        Book newBook = new BookBuilder().setTitle("Test Book").buildBook();
        this.bookService.save(newBook);
        Comment newComment = new CommentBuilder().setId(0L).setContent("Test content").buildComment();
        this.commentService.save(newBook.getId(), newComment);

        assertNotEquals(0L, newComment.getId());
    }

    @Test
    void given_new_comment_with_id_ok_wehn_save_then_new_comment_do_not_change_id(){
        Book newBook = new BookBuilder().setTitle("Test Book").buildBook();
        this.bookService.save(newBook);
        Comment newComment = new CommentBuilder().setId(100L).setContent("Test content").buildComment();
        this.commentService.save(newBook.getId(), newComment);

        assertEquals(100L, newComment.getId());
    }

    @Test
    void when_book_with_comments_should_return_all_the_comments_from_book(){
        Book newBook = new BookBuilder().setTitle("Test Book").buildBook();
        this.bookService.save(newBook);
        Comment newComment = new CommentBuilder().setContent("Test content").buildComment();
        this.commentService.save(newBook.getId(), newComment);

        Collection<Comment> comments = this.commentService.getAll(newBook.getId());

        assertEquals(1, comments.size());
    }
}
