package org.eyo.bookmanager.web;

import org.eyo.bookmanager.BookBuilder;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("test")
class WebBookTest extends WebTest {

    @Test
    void given_app_when_calling_books_should_return_empty_list() throws Exception {
        this.mockMvc.perform(get("/books"))
                .andExpect(status().isOk())
                .andExpect(content().json("[]"));
    }

    @Test
    void given_1_book_created_when_calling_books_should_return_list_with_one_element() throws Exception {
        Long bookId = createBook(new BookBuilder().setTitle("example").build());

        this.mockMvc.perform(get("/books"))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0].title").value("example"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.*").isArray())
                .andExpect(MockMvcResultMatchers.jsonPath("$", hasSize(1)));


        this.mockMvc.perform(delete("/books/" + bookId));
    }

    @Test
    void create_complex_book_should_return_ok_with_all_fields() throws Exception {
        String bookContent = new BookBuilder()
                .setAuthor("test_author")
                .setEditorial("test_editorial")
                .setReview("Some review for the book")
                .setTitle("test_title")
                .setYearPublication(1987L)
                .build();

        Long bookId = createBook(bookContent);

        this.mockMvc.perform(get("/books/" + bookId))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.yearPublication").value(1987))
                .andExpect(MockMvcResultMatchers.jsonPath("$.review").value("Some review for the book"));

        this.mockMvc.perform(delete("/books/" + bookId));
    }

    @Test
    void delete_no_existing_book_should_return_not_found() throws Exception {
        this.mockMvc.perform(delete("/books/12345667899")).andExpect(status().isNotFound());
    }
}
