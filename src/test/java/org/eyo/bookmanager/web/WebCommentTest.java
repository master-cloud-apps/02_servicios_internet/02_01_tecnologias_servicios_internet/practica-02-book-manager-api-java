package org.eyo.bookmanager.web;

import org.junit.jupiter.api.Test;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class WebCommentTest extends WebTest {

    @Test
    void given_no_comment_created_when_get_comment_should_return_not_found() throws Exception {
        this.mockMvc.perform(get("/books/1/comments/12345678"))
                .andExpect(status().isNotFound());
    }
}
