package org.eyo.bookmanager.web;

import org.eyo.bookmanager.UserBuilder;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class RestApiUserTest extends WebTest {

    @Test
    void given_app_when_calling_users_should_return_empty_list() throws Exception {
        this.mockMvc.perform(get("/users"))
                .andExpect(status().isOk())
                .andExpect(content().json("[]"));
    }

    @Test
    void given_1_user_created_when_calling_users_should_return_list_with_one_element() throws Exception {
        Long userId = createUser(new UserBuilder().setNick("nick1").setEmail("test_email@gmail.com").build());

        this.mockMvc.perform(get("/users"))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0].nick").value("nick1"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0].email").value("test_email@gmail.com"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.*").isArray())
                .andExpect(MockMvcResultMatchers.jsonPath("$", hasSize(1)));


        this.mockMvc.perform(delete("/users/" + userId));
    }

    @Test
    void create_user_should_return_ok_with_all_fields() throws Exception {
        Long userId = createUser(new UserBuilder().setNick("nick1").setEmail("test_email@gmail.com").build());

        this.mockMvc.perform(get("/users/" + userId))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.nick").value("nick1"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.email").value("test_email@gmail.com"));
        this.mockMvc.perform(delete("/users/" + userId));
    }

    @Test
    void create_two_users_same_nick_should_return_bad_response() throws Exception {
        Long userId = createUser(new UserBuilder().setNick("nick1").setEmail("test_email@gmail.com").build());
        String newUserSameNickJson = new UserBuilder().setNick("nick1").setEmail("test_email_2@gmail.com").build();

        this.mockMvc.perform(
                post("/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(newUserSameNickJson))
                .andExpect(status().isBadRequest());

        this.mockMvc.perform(delete("/users/" + userId));
    }

    @Test
    void when_get_user_not_created_should_return_not_found() throws Exception {
        this.mockMvc.perform(get("/users/1287522"))
                .andExpect(status().isNotFound());
    }

    @Test
    void given_created_user_should_return_ok_when_update_email() throws Exception {
        Long userId = createUser(new UserBuilder().setNick("nick1").setEmail("test_email@gmail.com").build());
        String userJson = new UserBuilder().setNick("nick1").setEmail("test_email_2@gmail.com").build();

        this.mockMvc.perform(
                put("/users/" + userId)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(userJson))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.nick").value("nick1"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.email").value("test_email_2@gmail.com"));


        this.mockMvc.perform(get("/users/" + userId))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.nick").value("nick1"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.email").value("test_email_2@gmail.com"));
        this.mockMvc.perform(delete("/users/" + userId));
    }

    @Test
    void given_no_user_created_when_update_user_should_return_not_found() throws Exception {
        String userJson = new UserBuilder().setNick("nick1").setEmail("test_email_2@gmail.com").build();

        this.mockMvc.perform(
                put("/users/1234567876543")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(userJson))
                .andExpect(status().isNotFound());
    }

    @Test
    void delete_no_existing_user_should_return_not_found() throws Exception {
        this.mockMvc.perform(delete("/users/12345667899")).andExpect(status().isNotFound());
    }
}
