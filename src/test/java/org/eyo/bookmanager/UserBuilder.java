package org.eyo.bookmanager;

import org.eyo.bookmanager.models.User;

public class UserBuilder {

    private String nick;
    private String email;

    public UserBuilder setNick(String nick) {
        this.nick = nick;
        return this;
    }

    public UserBuilder setEmail(String email) {
        this.email = email;
        return this;
    }

    public User buildUser(){
        User user = new User();
        user.setEmail(this.email);
        user.setNick(this.nick);
        return user;
    }

    public String build() {
        return "{" +
                "\"nick\": \"" + this.nick + "\"," +
                "\"email\": \"" + this.email + "\"" +
                "}";
    }
}
