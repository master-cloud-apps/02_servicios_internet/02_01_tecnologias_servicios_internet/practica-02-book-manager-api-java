package org.eyo.bookmanager.controllers;

import org.eyo.bookmanager.UserBuilder;
import org.eyo.bookmanager.api.BookAPI;
import org.eyo.bookmanager.api.CommentAPI;
import org.eyo.bookmanager.api.UserAPI;
import org.eyo.bookmanager.dtos.requests.BookRequestDTO;
import org.eyo.bookmanager.dtos.requests.UserRequestDTO;
import org.eyo.bookmanager.models.Book;
import org.eyo.bookmanager.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

@SpringBootTest
@ActiveProfiles(value = {"h2", "db"})
public abstract class ControllerTest {
    public static final String TEST_NICK = "test_nick";
    public static final String TEST_GMAIL_COM = "test@gmail.com";
    @Autowired
    protected BookAPI bookController;
    @Autowired
    protected CommentAPI commentController;
    @Autowired
    protected UserAPI userController;

    protected Long getIdFromLocation(String locationHeader) {
        return Long.parseLong(locationHeader.substring(locationHeader.lastIndexOf('/') + 1));
    }

    protected Long getIdFromResponse(ResponseEntity response){
        return this.getIdFromLocation(response.getHeaders().get("location").get(0));
    }

    protected void deleteBooks(List<Long> listIds){
        listIds.stream().forEach(this.bookController::deleteBookById);
    }

    protected UserRequestDTO createTestUser() {
        return this.createUser(TEST_GMAIL_COM ,TEST_NICK);
    }

    protected UserRequestDTO createUser(String email, String nick) {
        return new UserRequestDTO(new UserBuilder().setEmail(email).setNick(nick).buildUser());
    }

    protected BookRequestDTO createTestBook(Book book){
        return new BookRequestDTO(book);
    }
}
